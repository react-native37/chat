import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { store, persistor } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import AppNavigation from './src/AppNavigation';
import NavigationService from './src/NavigationService';
import messaging from '@react-native-firebase/messaging';
import { socketContext } from './src/socketContext';
import { chatApiUrl } from '@app/constants';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";

import { io } from 'socket.io-client';

const socket = io(chatApiUrl);

function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

// push notification service
PushNotification.configure({

  onNotification: function (remoteMessage) {
    console.log("NOTIFICATION:", remoteMessage);
    if (remoteMessage.data.from == "local") {
      
      NavigationService.navigate('ChatScreen', { id: remoteMessage.data.id, photo: "", name: remoteMessage.data.senderName, socket: socket })

    }
    remoteMessage.finish(PushNotificationIOS.FetchResult.NoData);
  },


  onAction: function (notification) {
    console.log("ACTION:", notification);
    let data= JSON.parse(notification.userInfo);
    console.log(data);
    let chatId= create_UUID()
    let message = {
      _id: chatId,
      conversation_id: data.conversationId,
      createdAt: new Date(),
      pending: true,
      receiver: data.sender ,//now receiver is sender
      sender: data.receiver,
      text: notification.reply_text,
      user: {
        _id: Number(data.receiver),
        name: data.name,
      }

    }
    console.log(message);
    socket.emit('send message', message);

  },


  onRegistrationError: function (err) {
    console.error(err.message, err);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  popInitialNotification: true,


  requestPermissions: true,
});

//end notification service


const App = () => {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <>
          <StatusBar barStyle="light-content" hidden={false} backgroundColor="#000000" translucent={false} />
          <socketContext.Provider value={socket}>
            <AppNavigation
              ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
              }}
            />
          </socketContext.Provider>
        </>
      </PersistGate>
    </Provider>

  )
}

export default App;