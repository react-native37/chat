/**
 * @format
 */
import 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import PushNotification from "react-native-push-notification";
import messaging from '@react-native-firebase/messaging';
PushNotification.createChannel(
    {
        channelId: "my_id", // (required)
        channelName: "Chat App", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        vibrate: true,
        importance:5
    },
    (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
);

messaging().setBackgroundMessageHandler(async (remoteMessage)=>{
    
    PushNotification.localNotification({
        id: remoteMessage.data.conversationId,
        channelId: "my_id",
        title: remoteMessage.data.title,
        message: remoteMessage.data.body,
        onlyAlertOnce: false,
        ignoreInForeground: true,
        priority: "max",
        actions: ["ReplyInput"],
        invokeApp: false,
        reply_placeholder_text: "Write your response...",
        reply_button_text: "Reply",
        vibrate: true,
        userInfo:{
            id: remoteMessage.data.sender,
            photo:"",
            name: remoteMessage.data.receiverName,
            senderName: remoteMessage.data.name,
            sender: remoteMessage.data.sender,
            receiver: remoteMessage.data.receiver,
            conversationId: remoteMessage.data.conversationId,
            from:'local'
        }
    });

    console.log('message',remoteMessage);
})

AppRegistry.registerComponent(appName, () => App);
