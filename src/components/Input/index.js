import React, {useState} from 'react';
import { Text, TextInput, View, TouchableOpacity, Image } from 'react-native';
import styles from "./styles";

TextInput.defaultProps.selectionColor = '#FF7B00'

export const InputField = (props) => {

    const { label, inputRef, onChangeText, autoCapitalize, value, keyboardType, blurOnSubmit, onSubmitEditing, returnKeyType } = props;

    return (
       
        <View style={styles.parent}>
            
            <Text style={styles.label}>{label}</Text>
           
            <TextInput
                autoCorrect={false}
                autoCapitalize={autoCapitalize}
                returnKeyType={returnKeyType}
                style={styles.input}
                onChangeText={onChangeText}
                value={value}
                keyboardType={keyboardType}
                blurOnSubmit={blurOnSubmit}
                ref={inputRef}
                onSubmitEditing={onSubmitEditing}
               
            />
        
        </View>
    )
}

export const PasswordField = ({ label, inputRef, autoCapitalize ,onChangeText, value, keyboardType, blurOnSubmit, onSubmitEditing }) => {
   
    const [isSecure, setSecure] = useState(true)

    return (
        <View style={styles.parent}>

            <Text style={styles.label}>{label}</Text>
           
            <TextInput
                autoCorrect={false}
                autoCapitalize={autoCapitalize}
                style={styles.password}
                onChangeText={onChangeText}
                value={value}
                keyboardType={keyboardType}
                blurOnSubmit={blurOnSubmit}
                secureTextEntry={isSecure}
                ref={inputRef}
                onSubmitEditing={onSubmitEditing}
                
            />

            <TouchableOpacity onPress={() => { setSecure(!isSecure)}} >
                {isSecure ?
                 <Image style={{width:24,height:24}} source={require('@app/assets/icons/visibility.png')} /> :
                    <Image style={{ width: 22, height: 16 }} source={require('@app/assets/icons/invisability.png')} />
                 }
            </TouchableOpacity>
        
        </View>
    )
}
