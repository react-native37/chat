import { StyleSheet } from 'react-native';
import { screenWidth } from "../../constants";

const styles = StyleSheet.create({
   
    parent: {
        backgroundColor: '#3A3A3C',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 8,
        borderRadius: 5,
        height:55,
        flexWrap: 'nowrap',
    },

    label: {
        width: '25%',
        color: '#A1A1A8',
        fontSize: 14
    },

    input: {
        color: '#fff',
        width: '75%',
        fontSize: 16
    },

    password: {
        color: '#fff',
        width: '68%',
        fontSize: 16
    }
});

export default styles;