import React from 'react';
import { Text,  TouchableOpacity,Image } from 'react-native';
import styles from './styles';

export const PrimaryButton = ({ title, action, titleStyle }) => {
    return (

        <TouchableOpacity style={styles.pButton} onPress={action}>
            <Text style={[styles.title, titleStyle]}>{title}</Text>
        </TouchableOpacity>

    )
}

export const SecondaryButton = ({ title, action, titleStyle }) => {
    return (

        <TouchableOpacity style={styles.sButton} onPress={action}>
            <Text style={[styles.title,titleStyle]}>{title}</Text>
        </TouchableOpacity>

    )
}

export const CustomButton = ({ title, action, btnStyle, titleStyle })=>(
    <TouchableOpacity style={btnStyle} onPress={action}>
        <Text style={[styles.title,titleStyle]}>{title}</Text>
    </TouchableOpacity>

)

export const BackButton = ({  action  }) => (
    <TouchableOpacity style={{ backgroundColor: '#FF7B00', borderRadius: 50, padding: 5 }} onPress={() => {action()}}>
        <Image style={styles.btn} source={require('@app/assets/icons/back.png')} />
    </TouchableOpacity>

)


