import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    pButton: {
        backgroundColor: '#FF7B00',
        height: 55,
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderRadius: 5
    },
    sButton: {
        backgroundColor: '#3A3A3C',
        height: '100%',
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        borderRadius: 5
    },
    title: {
        color: '#ffffff',
    },
    btn: {
        width: 15,
        height: 15,
    }
});

export default styles;