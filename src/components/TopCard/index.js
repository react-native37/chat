import React from "react";
import { View } from "react-native";
import styles from "./styles";

export default TopCard = ({children,custStyle,alignItem}) =>{
    return (

        <View style={[styles.container, custStyle, alignItem]}>
            {children}
        </View>

    );  
} 



