import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#3A3A3C',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    }

});

export default styles;