import { StyleSheet } from 'react-native';
import { screenWidth, screenHeight } from "../../constants";

const styles = StyleSheet.create({
   
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: '#0d0d0e69',
        position: 'absolute',
        zIndex: 5,
        height: "100%",
        width: "100%",

    },
});

export default styles;