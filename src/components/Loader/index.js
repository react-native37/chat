import React from "react";
import { ActivityIndicator, View } from "react-native";
import styles from "./styles";

export const Loader = () => (
    
    <View style={styles.container}>
        <ActivityIndicator size="large" color="#FF7B00" />
    </View>

);

export const FooterLoader = ({ isLoad }) => {
    if (isLoad == 1) {
        return (
            <View >
                <ActivityIndicator color="#FF7B00" size="large" />
            </View>
        )
    }else{
        return null;
    };
}


