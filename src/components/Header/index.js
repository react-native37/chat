import React from "react";
import { View } from "react-native";
import styles from "./styles";

export default  Header = (props) => (

    <View style={[styles.header,props.custStyle]}>
       
        <View style={props.leftChild && styles.left}>
            {props.leftChild}
        </View>
      
        <View>
            {props.rightChild}
        </View>
   
    </View>

);





