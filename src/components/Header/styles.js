import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  
    header: {
        display: 'flex',
        flexDirection: 'row',
        marginHorizontal: 10,
        marginTop: 10,
        alignItems: 'center',
        
    },

    left: {
        marginRight: 15,

    },
});

export default styles;