import React from 'react';
import { Text } from 'react-native';

export const LargText = ({ title, color }) => {
    return (
            <Text style={{fontSize:32,fontWeight:'bold',color:color}}>{title}</Text>
    )
}

export const MediumText = ({ title, color }) => {
    return (
        <Text style={{ fontSize: 22, color: color}}>{title}</Text>
    )
}

export const SmallText = ({ title, color }) => {
    return (
        <Text style={{ fontSize: 16, color: color }}>{title}</Text>
    )
}

export const CustomText = ({ title, color,size }) => {
    return (
        <Text style={{ fontSize: size, color: color }}>{title}</Text>
    )
}


