import React from 'react';
import { View } from 'react-native';

export const Margin5 = () => {
    return (
        <View style={{ marginVertical: 5 }}></View>
    )
}

export const Margin10 = () => {
    return (
        <View style={{ marginVertical: 10 }}></View>
    )
}

export const Margin12 = () => {
    return (
        <View style={{ marginVertical: 12 }}></View>
    )
}

export const Margin15 = () => {
    return (
        <View style={{ marginVertical: 15 }}></View>
    )
}

export const MarginV = ({ v }) => {
    return (
        <View style={{ marginVertical: v }}></View>
    )
}

export const MarginH = ({ h }) => {
    return (
        <View style={{ marginHorizontal: h }}></View>
    )
}

export const Margin = ({ v, h }) => {
    return (
        <View style={{ marginVertical: v, marginHorizontal: h }}></View>
    )
}
