import React, { useState, useCallback, useEffect, useRef } from 'react';
import { View, Image, AppState, Text, ActivityIndicator } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';//chat UI 
import { Header } from '@app/components';
import { CustomText } from '@app/components';
import { BackButton } from '@app/components';
import * as actions from './actions';
import { connect } from 'react-redux';
import _ from 'lodash';
import styles from "./styles";
import { NavigationEvents } from 'react-navigation';
import Clipboard from '@react-native-community/clipboard';

const mapToReceivedMsg = (messages) => {

    messages.map((val, i, arr) => {
        val.received = true;
        val.sent = true;
        val.pending = false;
        return messages;
    })

    return messages;
}

var getNameChar = function (string) {
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();

    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
};


const ChatScreen = (props) => {
console.log("in function");
  

    const { navigation, name, id, photo } = props;
    const chatWithId = Number(navigation.getParam('id'));
    const chatWithPhoto = navigation.getParam('photo');
    const chatWithName = navigation.getParam('name');
    const socket = navigation.getParam('socket');
    const [appState, setAppState] = useState(AppState.currentState);

    const [isUserOnline, setUserOnline] = useState(false);
    const [isTyping, setIsTyping] = useState(false);
    const [sent, setIsSent] = useState(false);
    const [force, forceUpdate] = useState(true);

    
    

    //generate conversation id
    const conversationId = Number([id, chatWithId].sort().join(''));
    const conversations = useRef(props.conversations[conversationId]);
    const [messages, setMessages] = useState(conversations.current);
    
    /*------------------------------------HANDLERS:BEGIN----------------------------------*/
    //setValue(name)
    const onDelete = (id) => {
        socket.emit('delete message', id, () => {
            setMessages(previousState => {
                return previousState.filter(message => message._id !== id)
            })
        })
    }
    
    

    const handleAppStateChange = (nextAppState) => {

        if (appState != nextAppState) {
            setAppState(nextAppState);
        }

        if (nextAppState === "active") {
            console.log("app state changd");

            socket.emit('enter to room', id, chatWithId, (res) => {
                props.getConversations(conversationId, (conversations) => {
                    setMessages(conversations)
                    socket.emit('received to end user', chatWithId, id, conversationId);
                })
            });

        } else {
            socket.emit('out from room', id);
        }

    };

    const onSend = useCallback((message = []) => {

        setIsTyping(false)
        message[0].pending = true;
        message[0].text = message[0].text.replace(/\n/g, " ");;
        setMessages(previousMessages => GiftedChat.append(previousMessages, message))
        message[0].receiver = chatWithId;//
        message[0].sender = id;
        message[0].conversation_id = conversationId;
        socket.emit('send message', message[0]);
        setIsSent(!sent)
        console.log(message[0]);

    }, [])

    

    /*------------------------------------HANDLERS:END----------------------------------*/

    /*------------------------------------HOOCKS:BEGIN----------------------------------*/
  
    //useEffect has no dependancy
    useEffect(() => {

  

        console.log("in use Effect");
        let isMounted = true

        socket.on('connect', () => {
            console.log("on connect");
            socket.emit('enter to room', id, chatWithId, () => {
                props.getConversations(conversationId, (val) => {
                    if (isMounted) {
                        setMessages(val)
                        socket.emit('received to end user', chatWithId, id, conversationId);
                    }

                })
            });

        })



        // user become online and received all sent message by end user
        socket.on("received to end user", (id) => {

            if (id == chatWithId) {
                if (isMounted) {
                    setMessages(previousMessages => {
                        return [...mapToReceivedMsg(previousMessages)]

                    })
                }
            }

        });


        //indicate end user is online or not
        socket.emit('check user online', chatWithId, id);

        socket.on("check user online", (bool, id) => {
            console.log("check user online");
            if (id == chatWithId) {
                if (isMounted) {

                    setUserOnline(bool)
                    if (!bool) {
                        setIsTyping(false)
                    }
                }
            }
        });

        //indicate end user typing message
        socket.on('typing message', function (isTyping, uid) {
            if (uid == chatWithId)
                if (isMounted) {
                    setIsTyping(isTyping)
                }
        })


        //call when end user send the message, sender received the message
        socket.on("send message", msg => {

            if (isMounted && msg.sender == chatWithId) {
                delete msg.receiver;
                delete msg.sender;
                delete msg.conversation_id;

                setMessages(previousMessages => GiftedChat.append(previousMessages, msg))
                socket.emit('message received', chatWithId, msg);
            }

        });

        if (isMounted) {
            AppState.addEventListener('change', handleAppStateChange);
        }


        return () => {
            isMounted = false;

           // AppState.removeEventListener('change', ()=>{});
        };
    }, [])

    //useEffect has 'sent' dependancy
    useEffect(() => {

        let isMounted = true

        //when current message seen by end user: make double tick 
        socket.on('message received', function (message) {
            if (isMounted) {
                setMessages(previousMessages => {
                    previousMessages[0] = message
                    return previousMessages
                })
                setIsSent(!sent)
            }
        })

        //message sent but not seen by end user:make single tick
        socket.on('message sent', function (message) {
            if (isMounted) {
                setMessages(previousMessages => {
                    previousMessages[0] = message
                    return previousMessages
                })
                setIsSent(!sent)
            }
        })

        return () => {
            isMounted = false;
        };


    }, [sent])

    /*------------------------------------HOOCKS:END----------------------------------*/

    return (

        <>
            <NavigationEvents
                onWillFocus={() => {
                    console.log("focus");
                    //user enter chat room
                    socket.emit('enter to room', id, chatWithId, () => {
                        //get chat conversation when user enter to the screen
                        props.getConversations(conversationId, (conversations) => {
                            setMessages(conversations)
                            socket.emit('check user online', chatWithId, id);
                            socket.emit('received to end user', chatWithId, id, conversationId);
                        })
                    });
                }}


            />
            <View style={{ backgroundColor: '#000' }}>

                <Header
                    leftChild={<BackButton action={() => navigation.goBack()} />}
                    rightChild={

                        <View style={styles.chatUserHead}>


                            {chatWithPhoto ? <Image style={styles.photo} resizeMode="cover" resizeMethod='scale' source={chatWithPhoto} /> : <View style={[styles.photoText, styles.photo]}><Text style={styles.profileText}>{getNameChar(chatWithName)}</Text></View>}

                            <View style={{ flexDirection: 'column' }}>
                                <CustomText title={chatWithName} color="#ffffff" size={16} />
                                {isUserOnline && <CustomText title="Online" color="#cfcfcf" size={12} />}
                            </View>
                        </View>

                    }
                    custStyle={{ marginBottom: 5 }}
                />

            </View>

            <View style={styles.main}>

                <GiftedChat
                    messages={messages}
                    onSend={messages => onSend(messages)}
                    alwaysShowSend={true}
                    user={{
                        _id: id,
                        name: name,
                    }}

                    inverted={true}

                    onInputTextChanged={(val) => {
                        if (val != "") {
                            socket.emit('typing message', true, chatWithId, id);
                        }
                        if (val.length == 0)
                            socket.emit('typing message', false, chatWithId, id);
                    }
                    }
                    isCustomViewBottom={true}
                    isTyping={isTyping}
                    scrollToBottom

                    scrollToBottomComponent={() => (
                        <View>
                            <Image style={{ width: 35 }} resizeMode='contain' source={require('@app/assets/icons/arrow-bottom.png')} />
                        </View>
                    )}
                    renderLoading={() => (
                        <View style={styles.randerLoading}>
                            <ActivityIndicator size='large' color='white' />
                        </View>
                    )}

                    onLongPress={(context, message) => {
                        const options = ['Copy', 'Delete Message', 'Cancel'];
                        const cancelButtonIndex = options.length - 1;
                        context.actionSheet().showActionSheetWithOptions({
                            options,
                            cancelButtonIndex
                        }, (buttonIndex) => {
                            switch (buttonIndex) {
                                case 0:
                                    Clipboard.setString(message.text);
                                    break;
                                case 1:
                                    onDelete(message._id) //pass the function here
                                    break;


                            }
                        });
                    }}

                />

            </View>
        </>

    )
}

const mapStateToProps = state => ({
    id: state.auth.userData.id,
    name: state.auth.userData.name,
    photo: state.auth.userData.photo,
    conversations: state.chat.conversations
})

const mapDispatchToProps = dispatch => ({
    getConversations: (data, callback) => dispatch(actions.getConversations(data, callback))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);


