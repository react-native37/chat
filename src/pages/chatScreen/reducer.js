const INITIAL_STATE = {
    conversations: {},
    isLodaing: true
}

const chat = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case 'GET_CONVERSATION_ATTEMPT':

            return {
                ...state,
                isLoading: true
            }
            break;
        case 'GET_CONVERSATION_SUCCESS':
            let newConversation = {};
            newConversation[action.id] = action.conversations;
            return {
                ...state,
                isLoading: false,
                conversations: { ...state.conversations, ...newConversation }
            }
            break;
        // case 'ADD_NEW_MESSAGE':
        //     console.log(state.conversations[action.id]);
        //     if (!state.conversations[action.id]) {
        //         var a = [action.message]
        //         console.log(a);
        //     } else {
        //         var a = state.conversations[action.id];
        //         a.unshift(action.message);
        //         console.log(a);

        //     }

        //     return {
        //         ...state,
        //         conversations: { ...state.conversations, ...a }

        //     }

        default:
            return state
    }
}

export default chat;