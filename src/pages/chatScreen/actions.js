import api from '@app/redux/api';
import { ToastAndroid } from 'react-native';

const displayMessage = (message) => {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        150
    );
}


export const onMessageReceivedAction = (message) => {
    return { type: 'MESSAGE_RECEIVED', message };
};

export const getConversationAttempt = () => {
    return {
        type: 'GET_CONVERSATION_ATTEMPT',
    }
}

export const getConversationSuccess = (data, id) => {
    return {
        type: 'GET_CONVERSATION_SUCCESS',
        conversations: data,
        id

    }
}




export const getConversations = (conversationId, callback) => async (dispatch) => {

    dispatch(getConversationAttempt());

    try {

        const response = await api.get(`/get-conversation/${conversationId}`);

        const data = response.data;
        const status = data.status;
        const result = data.conversations;

        if (status == 1) {


            let conversations = [];
            result.forEach(function (val, index, arr) {

                conversations.push(JSON.parse(val.message))

                if (index + 1 == result.length) {
                    callback(conversations);
                    dispatch(getConversationSuccess(conversations, conversationId))
                    //      console.log(conversations);
                } else {
                    return val;
                }

            })
        }

    } catch (err) {
        console.log(err);
        //    displayMessage("No internet connection")

    }


}


// export const addNewMessage = (id, message) => {
//     return {
//         type: 'ADD_NEW_MESSAGE',
//         message,
//         id

//     }
// }

// export const setNewMessage = (id, message) => (dispatch) => {
//     try {


//         dispatch(addNewMessage(id, message))
//     } catch (e) {
//         console.log(e);
//     }
// }


