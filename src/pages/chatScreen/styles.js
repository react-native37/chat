import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   
    chatUserHead: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: "wrap",
        marginVertical: 5
    },

    photo: {
        width: 50,
        height: 50,
        marginRight: 12,
        borderRadius: 25,
        borderWidth: 3,
        borderColor: '#FF7B00'
    },

    photoText: {
        backgroundColor: '#0a3a3a',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileText: {
        fontWeight: 'bold',
        color: '#fff'
    },

    main: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-start',
        paddingTop: 0,
        backgroundColor: '#3A3A3C',

    },
    randerLoading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }

});

export default styles;