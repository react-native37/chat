const INITIAL_STATE = {
    isLoading: false,
}

const forgotPassword = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case 'FORGOTPASS_ATTEMPT':
            return {
                ...state,
                isLoading: action.isLoading,
               
            }
            break;
        default:
            return state
    }
}

export default forgotPassword;