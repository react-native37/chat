import React, {  useState } from 'react';
import { View, Image, ImageBackground } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';

//components
import { PrimaryButton } from '@app/components';
import { LargText, SmallText } from '@app/components';
import { InputField } from '@app/components';
import { Margin10, MarginV } from '@app/components';
import { Loader } from '@app/components';

import styles from "./styles";

import * as action from './actions';

const clearInputs = (setEmail) => {
    setEmail('');
}


const ForgotPassword = (props) => {

    const [email, setEmail] = useState('');

    return (
        <View style={styles.parent}>
            
            {props.isLoading && <Loader />} 

         {/* clear inputs when leave the screen */}
            
            <NavigationEvents
                onDidBlur={() => clearInputs(setEmail)}
            />

            <ImageBackground source={require('../../assets/icons/left-art.png')} imageStyle={styles.topImage} style={styles.header} >
                <Image
                    style={styles.logo}
                    source={require('../../assets/icons/logo.png')}
                />
            </ImageBackground>
         
            <MarginV v={15} />
        
            <View style={styles.body}>
                <View>
                    <KeyboardAwareScrollView>

                        <LargText title="FORGOT PASSWORD" color="#fff" />

                        <Margin10 />

                        <SmallText title="Please enter your registered email address" color="#fff" />

                        <Margin10 />

                        <InputField
                           
                           label="Email"
                            value={email}
                            autoCapitalize='none'
                            keyboardType='default'
                            blurOnSubmit={true}
                            onChangeText={(value) => { setEmail(value) }}
                            onSubmitEditing={() => { props.forgotPassword(email)}}
                        
                        />

                        <Margin10 />

                        <SmallText title="Don't Worry! Just fill in your email and we will send a link to reset password." color="#fff" />
                        
                        <Margin10 />
                       
                        <View style={styles.buttonContainer}>
                            <PrimaryButton title="Reset Password" titleStyle={{fontSize:16}} action={() => { props.forgotPassword(email) }} />
                        </View>

                        <Margin10 />

                        <TouchableOpacity onPress={() => { props.navigation.navigate('Login') }} style={styles.signupLink}>
                            <SmallText title="Back to?" color="#fff" /><SmallText title=" Sign In" color="#FF7B00" />
                        </TouchableOpacity>

                    </KeyboardAwareScrollView>
                    <Margin10 />
                </View>
           
            </View>

        </View>
    )
}

const mapStateToProps = state => ({
    isLoading: state.forgotPassword.isLoading,
})

const mapDispatchToProps = dispatch => ({
    forgotPassword: (email) => dispatch(action.forgotPassword(email))
})

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);

