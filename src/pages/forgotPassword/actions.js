
import api from '@app/redux/api';
import { ToastAndroid } from 'react-native';

export function isLoading(bool) {
    return {
        type: 'FORGOTPASS_ATTEMPT',
        isLoading: bool
    }
}

const displayMessage = (message) => {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        150
    );
}

export const forgotPassword = (email) => async (dispatch) => {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (email.length == "") {

        displayMessage("Please enter email address");

    } else if (reg.test(email) == false) {

        displayMessage("Please enter valid email address");

    }  else {

        dispatch(isLoading(true));

        try {

            const response = await api.post('/forgot_password', {
                email
            });

            const message = response.data.message;

            dispatch(isLoading(false));

            displayMessage(message);

        } catch (err) {

            displayMessage("No internet connection")

        }
    }

}


