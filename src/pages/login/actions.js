import messaging from '@react-native-firebase/messaging';
import api from '@app/redux/api';
import { ToastAndroid } from 'react-native';

import NavigationService from '@app/NavigationService';

export function loginAttempt() {
  return {
    type: 'LOGIN_ATTEMPT'
  }
}

export function loginSuccess(userData) {
  return {
    type: 'LOGIN_SUCCESS',
    userData
  }
}

export function loginFailed(error) {
  return {
    type: 'LOGIN_FAILED',
    error
  }
}

const displayMessage = (message) => {
  ToastAndroid.showWithGravityAndOffset(
    message,
    ToastAndroid.LONG,
    ToastAndroid.BOTTOM,
    0,
    150
  );
}

export const login = ({ email, password }) => async (dispatch) => {


  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (email.length == "") {

    displayMessage("Please enter email address");

  } else if (reg.test(email) == false) {

    displayMessage("Please enter valid email address");

  } else if (password == "") {

    displayMessage("Please enter password");

  } else {

    dispatch(loginAttempt());

    try {

      const authorizationStatus = await messaging().requestPermission();

      if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
        console.log('User has notification permissions enabled.');
      } else if (authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL) {
        console.log('User has provisional notification permissions.');
      } else {
        console.log('User has notification permissions disabled');
      }  


      // Register the device with FCM
      await messaging().registerDeviceForRemoteMessages();

      // Get the token
      const fcmToken = await messaging().getToken();



      const response = await api.post('/signin', {
        email, password, fcmToken
      });

      const data = response.data.data;
      const status = response.data.status;
      const message = response.data.message;



      displayMessage(message);

      if (status == 1) {

        dispatch(loginSuccess(data));

        NavigationService.navigate('HomeStack');

      } else if (status == 0) {

        dispatch(loginFailed(message))

      }

    } catch (err) {

      dispatch(loginFailed(err))

      displayMessage("No internet connection")

    }
  }

}

const isLogout = () => {
  return {
    type: 'LOGOUT'
  }
}

export const logout = (id) => async (dispatch) => {

  const response = await api.post('/logout', {
    id
  });
  let status = response.data.status;
  let message = response.data.message;
  if (status == 1) {
    dispatch(isLogout());
    displayMessage(message);
    NavigationService.navigate('AuthStack');
  }

}