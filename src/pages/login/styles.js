import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   
    parent: {
        backgroundColor: '#000000',
        flex: 1,
        justifyContent: 'flex-start'
    },
    header: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 220,
    },
    topImage: {
        width: 135, height: 182,
    },
    logo: {
        width: 150,
        height: 150,

    },
    body: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20,

    },

    buttonContainer: {
        width: '100%',
        height: 55,

    },
    forgotLink: {
        alignSelf: 'flex-end'
    },
    signupLink: {
        flexDirection: 'row',
        alignSelf: 'center'
    },

    hrLine: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    line: {
        borderWidth: 1,
        borderBottomColor: '#A1A1A8',
        width: '100%',
        alignSelf: 'center'
    }

});

export default styles;