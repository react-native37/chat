import React, { useRef, useState } from 'react';
import { View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationEvents } from 'react-navigation';

//components
import { PrimaryButton } from '@app/components';
import { LargText, SmallText } from '@app/components';
import { InputField, PasswordField } from '@app/components';
import { MarginV } from '@app/components';
import { Loader } from '@app/components';

import styles from "./styles";

import { connect } from 'react-redux';
import * as actions from './actions';

const clearInputs = (inputFun) => {
    inputFun.setEmail('');
    inputFun.setPassword('');
}

const Login = (props) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    var PasswordRef = useRef(null)


    return (

        <View style={styles.parent}>

            {props.isLoading && <Loader />}

            {/* clear inputs when user leave the screen */}

            <NavigationEvents
                onDidBlur={() => clearInputs({ setEmail, setPassword })}
            />

            <ImageBackground source={require('../../assets/icons/left-art.png')} imageStyle={styles.topImage} style={styles.header} >
                <Image
                resizeMode='contain'
                    style={styles.logo}
                    source={require('../../assets/icons/logo.png')}
                />
            </ImageBackground>

            <MarginV v={15} />

            <View style={styles.body}>
                
                <View>
                
                    <KeyboardAwareScrollView >

                        <LargText title="SIGN IN" color="#fff" />

                        <MarginV v={5} />

                        <SmallText title="Please sign in to continue using the app " color="#fff" />

                        <MarginV v={8} />

                        <InputField
                            label="Email"
                            value={email}
                            autoCapitalize='none'
                            returnKeyType='next'
                            keyboardType='default'
                            onChangeText={(value) => { setEmail(value) }}
                            blurOnSubmit={false}
                            onSubmitEditing={() => { PasswordRef.focus(); }}
                        />

                        <MarginV v={5} />

                        <PasswordField
                            label="Password"
                            autoCapitalize='none'
                            onChangeText={(value) => { setPassword(value) }}
                            value={password}
                            keyboardType='default'
                            inputRef={(input) => { PasswordRef = input }}
                            onSubmitEditing={() => { props.login(email, password) }}
                        />
                        <MarginV v={5} />

                        {/* <TouchableOpacity onPress={() => { props.navigation.navigate('ForgotPassword') }} style={styles.forgotLink}>
                            <SmallText title="Forgot password?" color="#fff" />
                        </TouchableOpacity> */}

                        <MarginV v={5} />

                        <View style={styles.buttonContainer}>
                            <PrimaryButton title="Login" titleStyle={{ fontSize: 16 }} action={() => { props.login(email, password) }} />
                        </View>

                        <MarginV v={5} />

                        <TouchableOpacity onPress={() => { props.navigation.navigate('Signup') }} style={styles.signupLink}>
                            <SmallText title="Dont't have an account?" color="#fff" /><SmallText title=" Sign Up" color="#FF7B00" />
                        </TouchableOpacity>

                        <MarginV v={8} />
                    </KeyboardAwareScrollView>

                </View>

            </View>

        </View>
    )
}


const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn,
    isLoading: state.auth.isLoading,
    userData: state.auth.userData,
})

const mapDispatchToProps = dispatch => ({
    login: (email, password) => dispatch(actions.login({ email, password }))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)

