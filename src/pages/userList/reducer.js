const INITIAL_STATE = {
    isLoading: false,
    isNextAvailable: 0,
    UserList: [],
    isRefresh:false,
    canSearch:true
}

const userList = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case 'USERLIST_ATTEMPT':
            return {
                ...state,
                isLoading:true,
                canSearch: true
            }
            break;
        case 'USERLIST_SUCCESS':
            let UserList = action.isRefresh ? action.UserList : state.UserList.concat(action.UserList);

            return {
                ...state,
                isLoading:false,
                isNextAvailable: action.isNextAvailable,
                UserList: UserList,
                isRefresh:action.isRefresh,
                canSearch: true
            }
            break;

        case 'SEARCH_USER_ATTEMPT':
            return {
                ...state,
                isLoading: true,
                isRefresh:false,
                canSearch: false
            }
            break;
        case 'SEARCH_USER_SUCCESS':

            return {
                ...state,
                isLoading: false,
                isRefresh: false,
                UserList: action.UserList,
                canSearch: false
            }
            break;
        case 'USERLIST_FAILED':

            return {
                ...state,
                isLoading: false,
                isRefresh: false,
                canSearch: false
            }
            break;
            
        default:
            return state
    }
}

export default userList;


