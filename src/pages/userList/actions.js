import api from '@app/redux/api';
import { ToastAndroid } from 'react-native';

export function userListAttempt() {
    return {
        type: 'USERLIST_ATTEMPT',
    }
}

export function userListSuccess(data, isRefresh) {
    return {
        type: 'USERLIST_SUCCESS',
        UserList: data.userList,
        isNextAvailable: data.isNextPageAvailable,
        isRefresh
    }
}

export function userListFailed() {
    return {
        type: 'USERLIST_FAILED',
    }
}


const displayMessage = (message) => {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        150
    );
}

export const getUserList = ({ page, limit, userId, isRefresh,callback }) => async (dispatch) => {

    dispatch(userListAttempt());

    try {

        const response = await api.post('/get-users', {
            page, limit, userId
        });

        const data = response.data;
        callback(data.userList)
        dispatch(userListSuccess(data, isRefresh))
       

    } catch (err) {
        console.log(err);
        dispatch(userListFailed());
        displayMessage("No internet connection")

    }
}

// search user


export function searchUserAttempt() {
    return {
        type: 'SEARCH_USER_ATTEMPT',

    }
}

export function searchSuccess(data) {
    return {
        type: 'SEARCH_USER_SUCCESS',
        UserList: data.userList,
    }
}




export const getSearchUser = ({ title, userId,callback }) => async (dispatch) => {

    dispatch(searchUserAttempt());
    try {

        const response = await api.post('/search-users', {
            title, userId
        });

        const data = response.data;
        
        callback(data.userList)
        if(data.userList.length==0){
            displayMessage("Search user not found")
        }
        dispatch(searchSuccess(data))
        


    } catch (err) {
        dispatch(userListFailed());

       // displayMessage("No internet connection")

    }
}

// export const sortUserList = (data) => {
//     return {
//         type: 'UPDATE_USERLIST',
//         UserList: data
//     }

// }

// export const setSortedList = (list) => async (dispatch) => {
//     dispatch(sortUserList(list))
// }
