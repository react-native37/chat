import { StyleSheet } from 'react-native';
import { screenWidth } from '../../constants';

const styles = StyleSheet.create({

    parent: {
        backgroundColor: '#000000',
        flex: 1,
        justifyContent: 'flex-start'
    },


    body: {
        flex: 1,
        justifyContent: 'flex-end'
    },

    title: {
        color: '#fff',
        fontSize: 22,
        paddingHorizontal: screenWidth * 0.065,
        paddingVertical: screenWidth * 0.020,
        fontWeight: 'bold'
    },

    main: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-start',
        paddingTop: 0,
        paddingHorizontal: 5,
        backgroundColor: '#3A3A3C',
    },
    cardContainer: {
        flexDirection: "row",
        marginVertical: 15,
        alignItems: "center",

        // justifyContent: 'center'
    },
    photo: {
        width: 55,
        height: 55,
        marginHorizontal: 12,
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#FF7B00'
    },

    photoText: {
        backgroundColor: '#0a3a3a',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileText: {
        fontWeight: 'bold',
        color: '#fff'
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff',

    },
    lastMsg: {
        fontSize: 12,
        color: '#b1b1b1',
        width: screenWidth - 150,

    },
    time: {
        color: "#ffffff",
        fontSize: 12
    },
    titleContainer: {
        width: screenWidth - 100,
        flexWrap: "wrap",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",

    },

    innerTitleContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        width: "100%",
        justifyContent: "space-between"
    },
    
    saperator: {
        borderBottomWidth: 0.5,
        borderColor: '#fff',
        width: screenWidth * 0.8,
        alignSelf: 'flex-end'
    },

    searchContainer: {
        backgroundColor: '#505051',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 5,
        justifyContent: 'space-between',
        marginVertical: 5,
        paddingHorizontal: 10,
        // marginHorizontal: 15,
        width: screenWidth * 0.75,
        height: 40
    },
    searchIcon: {
        width: 18,
        height: 18
    },
    clearIcon: {
        width: 14,
        height: 14
    },

    searchInput: {
        color: '#fff',
        width: screenWidth * 0.5,
        fontSize: 14,
    },

    // headerLeft: {
    //     flexDirection: "row",
    //     width: screenWidth * 0.7,
    //     justifyContent: "space-between",
    //     flexWrap:'wrap',
    //     alignItems:'center'
    // },
    logout: {
        color: "#fff"
    },
    onlineDot: {
        width: 12,
        height: 12,
        borderRadius: 50,
        backgroundColor: "#32CD32",
        position: 'absolute',
        left: 55,
        top: 36,
        borderWidth: 2,
        borderColor: '#ffffff'
    },

    unreadMsgBubble: {
        padding: 5,
        justifyContent: 'center',
        height: 20,
        borderRadius: 45,
        marginRight: 2,
        backgroundColor: "#32CD32",
        borderColor: '#fff',
        borderWidth: 1,
        alignItems: 'center'
    },
    unreadMsgCount: {
        color: '#ffffff',
        fontSize: 14
    }
});

export default styles