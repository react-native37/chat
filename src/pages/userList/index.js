import React, { useRef, useState, useEffect, useContext } from 'react';
import { View, Image, TouchableOpacity, Text, FlatList, TextInput, AppState } from 'react-native';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import moment from 'moment'
import { socketContext } from '@app/socketContext';
//components
import { Header } from '@app/components';
import { Loader, FooterLoader } from '@app/components';
import { chatApiUrl } from '@app/constants';
import { userPhotoUrl } from '@app/constants';

import styles from "./styles";
import * as getAction from './actions';
import * as searchAction from './actions';
import * as logoutAction from '@app/pages/login/actions';
import PushNotification from "react-native-push-notification";

const sortUserListByLatestMsg = (users, id, callback) => {

    users.forEach((val, index, array) => {
        if (val.id == id) {
            var element = array[index];
            array.splice(index, 1);
            array.splice(0, 0, element);

            callback(array);

        }
    })

}

var getNameChar = function (string) {
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();

    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
};


const UsersScreen = (props) => {

    const socket = useContext(socketContext);

    const [allConnectedUsers, setConnectedUsers] = useState({});
    const [appState, setAppState] = useState(AppState.currentState);
    const [unreadMsgCount, setUnreadMsgCount] = useState({});
    const [msgText, setMegText] = useState({});//store last received message
    const [page, setPage] = useState(1);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [searchTitle, setSearchTitle] = useState("");
    const [firstRender, setFirstRender] = useState(true);


    let updatedListRef = useRef(props.userList);

    const [updatedList, setUpdatedList] = useState(updatedListRef.current);

    let inputRef = useRef(null);

    const userId = props.userId;
    const limit = 30;

    const getuser = () => {
        let promise = new Promise((resolve) => {
            resolve(props.getUserList(1, limit, userId, true, (userdata) => {
                updatedListRef.current = userdata;
                setUpdatedList([...userdata])


            }))
        })
        return promise;
    }



    useEffect(() => {



        let isMounted = true;

        if (firstRender) {
            let promise = getuser();
            promise.then((res) => {
                console.log("message count");
                socket.emit('connect user', userId);
                socket.emit('received message count', userId);
                if (isMounted) {
                    setFirstRender(false);
                    // setUserList(props.userList);
                }
            })

        }
        socket.on("connect", () => {

            socket.emit('connect user', userId);
        });



        //get all online users

        socket.on('get connected users', (users) => {
            if (isMounted) {
                setConnectedUsers(users);
            }
        })


        //unread message count of all end users who send message


        socket.on('received message count', (countArr) => {

            if (isMounted) {
                setUnreadMsgCount(countArr);
            }
        })


        //when new message arrived: user not in chat room but in user list

        socket.on('new message', (senderId, msg) => {

            if (isMounted) {
                setUnreadMsgCount(previousState => {
                    if (!isNaN(previousState[senderId]) && Object.keys(previousState).length !== 0) {
                        previousState[senderId] = previousState[senderId] + 1;
                        return { ...previousState };
                    } else {
                        previousState[senderId] = 1;
                        return { ...previousState };
                    }

                })
                setMegText(previousMsg => {
                    previousMsg[senderId] = { text: msg, time: moment.utc(new Date()).utcOffset(330).format("hh:mm a") };

                    return { ...previousMsg }
                })//set lase received message
            }
            sortUserListByLatestMsg(updatedListRef.current, senderId, (data) => {

                if (isMounted) {
                    setUpdatedList([...data]);
                }
            })
        })

        //set last message to user when user in chat room

        socket.on('set message to user', (senderId, msg) => {

            if (isMounted) {
                setMegText(previousMsg => {
                    previousMsg[senderId] = { text: msg, time: moment.utc(new Date()).utcOffset(330).format("hh:mm a") };

                    return { ...previousMsg }
                })//set lase received message
            }
            sortUserListByLatestMsg(updatedListRef.current, senderId, (data) => {
                if (isMounted) {

                    setUpdatedList([...data]);
                }
            });
        })



        //make connect or disconnect when app open or close

        AppState.addEventListener('change', handleAppStateChange);

        return () => {
            isMounted = false;
            AppState.removeEventListener('change', handleAppStateChange);
        }

    }, []);


    const handleAppStateChange = async (nextAppState) => {

        if (appState != nextAppState) {
            setAppState(nextAppState);
        }

        if (nextAppState === "active") {
            PushNotification.cancelAllLocalNotifications();
            //connect to socket
            //  await notifee.cancelNotification("123");
            let promise = getuser();
            promise.then((res) => {
                socket.emit('connect user', userId);
                socket.emit('received message count', userId);



            })

        } else {
            setMegText({});
            socket.emit('disconnect user', userId, () => { });
        }

    };

    return (

        <View style={styles.parent}>
            <NavigationEvents onWillFocus={() => {
                if (!firstRender) {
                    socket.emit('out from room', userId);
                    socket.emit('received message count', userId)
                }

            }}

            />
            <Header

                leftChild={
                    <View>
                        <View style={styles.searchContainer}>

                            <TextInput

                                autoCorrect={false}
                                onChangeText={(val) => setSearchTitle(val)}
                                style={styles.searchInput}
                                blurOnSubmit={true}
                                ref={inputRef}
                                onSubmitEditing={(val) => {
                                    if (searchTitle != "") {
                                        props.getSearchUser(searchTitle, userId, (userdata) => {
                                            setUpdatedList([...userdata])
                                        })
                                    }
                                }}
                                placeholder="Search user"
                                placeholderTextColor="#fff" />

                            <TouchableOpacity onPress={() => {
                                if (props.canSearch && searchTitle != "")
                                    props.getSearchUser(searchTitle, userId, (userdata) => {
                                        setUpdatedList([...userdata])
                                    })
                                else {
                                    props.getUserList(1, limit, userId, true, (userdata) => {
                                        setUpdatedList([...userdata])
                                    });
                                    inputRef.current.clear();
                                }

                            }}>
                                {props.canSearch ?
                                    <Image style={styles.searchIcon} resizeMode="contain" source={require('../../assets/icons/search.png')} /> :
                                    <Image style={styles.clearIcon} resizeMode="contain" source={require('../../assets/icons/close.png')} />
                                }
                            </TouchableOpacity>

                        </View>


                    </View>
                }

                rightChild={
                    <TouchableOpacity onPress={() => {
                        socket.emit('disconnect user', userId, () => {
                            props.logout(userId);
                        })
                    }}>
                        <Text style={styles.logout}>Logout</Text>
                    </TouchableOpacity>
                }

                custStyle={{ marginBottom: 5, justifyContent: 'space-between' }} />


            <View style={styles.body}>
                {page == 1 && props.isLoading && !isRefreshing && <Loader />}

                <View style={styles.main}>

                    <FlatList

                        ListFooterComponent={<FooterLoader isLoad={props.isNextAvailable} />}
                        data={updatedList}
                        extraData={[allConnectedUsers, unreadMsgCount, msgText, updatedList]}
                        keyExtractor={(item) => String(item.id)}
                        renderItem={({ item }) => {

                            let photo = item.photo ? { uri: `${userPhotoUrl}/${item.photo}` } : null;

                            return (

                                <TouchableOpacity
                                    style={styles.cardContainer}
                                    onPress={() => {

                                        setUnreadMsgCount(countArr => {
                                            console.log(countArr);
                                            delete countArr[item.id];
                                            return { ...countArr };
                                        });
                                        props.navigation.navigate('ChatScreen', { id: item.id, photo: photo, name: item.name, socket: socket })
                                    }

                                    }


                                >
                                    {photo ? <Image style={styles.photo} source={photo} /> : <View style={[styles.photoText, styles.photo]}><Text style={styles.profileText}>{getNameChar(item.name)}</Text></View>}

                                    {/*  if user online, display green dot on photo */}
                                    { (item.id in allConnectedUsers) && <View style={styles.onlineDot}></View>}

                                    {/* <View style={{justifyContent:'center'}}> */}
                                    <View style={styles.titleContainer}>
                                        <View style={styles.innerTitleContainer}>
                                            <Text numberOfLines={1} style={styles.title}>{item.name}</Text>
                                            {(item.id in msgText) ? <Text style={styles.time}>{msgText[item.id].time}</Text> : <Text style={styles.time}>{item.created_at}</Text>}
                                        </View>
                                        <View style={styles.innerTitleContainer}>
                                            {(item.id in msgText) ? <Text numberOfLines={1} style={styles.lastMsg}>{msgText[item.id].text}</Text> : <Text numberOfLines={1} style={styles.lastMsg}>{item.message != null && item.message}</Text>}

                                            {(item.id in unreadMsgCount) && <View style={styles.unreadMsgBubble}><Text style={styles.unreadMsgCount}>{
                                                unreadMsgCount[item.id]}</Text></View>}
                                        </View>
                                    </View>
                                    {/* last message */}


                                    {/* </View> */}

                                </TouchableOpacity>
                            );
                        }}


                        ItemSeparatorComponent={() => <View style={styles.saperator}></View>}

                        onEndReachedThreshold={0.1}

                        onEndReached={() => {
                            if (props.isNextAvailable === 1 && !isRefreshing && !props.isLoading) {
                                setPage(page + 1);
                                props.getUserList(page + 1, limit, userId, false, (userdata) => {
                                    //    setUpdatedList([...userdata])
                                });
                            }
                        }}

                        onRefresh={async () => {
                            setPage(1);
                            setIsRefreshing(true);
                            await props.getUserList(1, limit, userId, true, (userdata) => {
                                setUpdatedList([...userdata])
                            });
                            setIsRefreshing(false);
                            if (inputRef.current != null)
                                inputRef.current.clear();
                        }}
                        refreshing={isRefreshing} />

                </View>

            </View>

        </View>
    );
}


const mapStateToProps = state => ({

    userList: state.userList.UserList,
    isLoading: state.userList.isLoading,
    isRefresh: state.userList.isRefresh,
    canSearch: state.userList.canSearch,
    isNextAvailable: state.userList.isNextAvailable,
    userId: state.auth.userData.id,

})

const mapDispatchToProps = dispatch => ({
    getUserList: (page, limit, userId, isRefresh, callback) => dispatch(getAction.getUserList({ page, limit, userId, isRefresh, callback })),
    getSearchUser: (title, userId, callback) => dispatch(searchAction.getSearchUser({ title, userId, callback })),
    logout: (id) => dispatch(logoutAction.logout(id)),

})


export default connect(mapStateToProps, mapDispatchToProps)(UsersScreen)
