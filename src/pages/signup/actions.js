import api from '@app/redux/api';
import { ToastAndroid } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import NavigationService from '@app/NavigationService';

export const signupAttempt = () => {
    return {
        type: 'SIGNUP_ATTEMPT',
 
    }
}

export const signupSuccess = (data)=>{
    return {
        type:'SIGNUP_SUCCESS',
        userData:data
    }
}

export const signupFail = (error) => {
    return {
        type: 'SIGNUP_SUCCESS',
        error
    }
}

const displayMessage = (message) => {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        150
    );
}

export const signup = ({ name , email, password }) => async (dispatch) => {
    
 

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (name.length == "") {
    
        displayMessage("Please enter your name");

    } else if (email.length == "") {

        displayMessage("Please enter email address");

    } else if (reg.test(email) == false) {

        displayMessage("Please enter valid email address");

    } else if (password == "") {

        displayMessage("Please enter password");

    } else {

        dispatch(signupAttempt());

        try {

            const authorizationStatus = await messaging().requestPermission();

            if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
                console.log('User has notification permissions enabled.');
            } else if (authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL) {
                console.log('User has provisional notification permissions.');
            } else {
                console.log('User has notification permissions disabled');
            }  


            // Register the device with FCM
            await messaging().registerDeviceForRemoteMessages();

            // Get the token
            const fcmToken = await messaging().getToken();
            
            const response = await api.post('/signup', {
               name, email, password, fcmToken
            });

            const data = response.data.data;
            const status = response.data.status;
            const message = response.data.message;

            displayMessage(message);

            if (status == 1) {

                dispatch(signupSuccess(data))
                NavigationService.navigate('HomeStack'); 

            } else if (status == 0) {

                dispatch(signupFail(message))

            }

        } catch (err) {

            dispatch(signupFail(err))

            console.log(err);

            displayMessage("No internet connection")

        }
    }

}
