const INITIAL_STATE = {
    isLoggedIn: false,
    isLoading: false,
    userData: {},
    error: undefined
}

const auth = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case 'SIGNUP_ATTEMPT':
            return {
                ...state,
                isLoading: true,
                isLoggedIn: false
            }
            break;
        case 'SIGNUP_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isLoggedIn: true,
                userData: action.userData,
                error: undefined
            }
            break;
        case 'SIGNUP_FAILED':
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
                error: action.error
            }
            break;
        case 'LOGIN_ATTEMPT':
            return {
                ...state,
                isLoading: true,
                isLoggedIn: false
            }
            break;
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isLoggedIn: true,
                userData: action.userData,
                error: undefined
            }
            break;
        case 'LOGIN_FAILED':
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
                error: action.error
            }
            break;
        case 'LOGOUT':
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
                userData: {}
            }
            break;
        default:
            return state

    }
}

export default auth;