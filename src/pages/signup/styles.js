import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   
    parent: {
        backgroundColor: '#000000',
        flex: 1,
        justifyContent: 'flex-start'
    },
    header: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 220,
    },
    topImage: {
        width: 135, height: 182,
    },
    logo: {
        width: 150, height: 150,
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20,
    },

    buttonContainer: {
        width: '100%',
        height: 55,

    },

    signinLink: {
        flexDirection: 'row',
        alignSelf: 'center'
    }


});

export default styles;