import React, { useRef, useState } from 'react';
import { View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationEvents } from 'react-navigation';

// components
import { PrimaryButton } from '@app/components';
import { LargText, SmallText } from '@app/components';
import { InputField, PasswordField } from '@app/components';
import { MarginV } from '@app/components';
import { Loader } from '@app/components';

import styles from "./styles";

import {connect} from 'react-redux';
import * as actions from './actions';


const clearInputs = (inputFun) => {
    inputFun.setEmail('');
    inputFun.setName('');
    inputFun.setPassword('');
}


const Signup = (props) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    
    var EmailRef = useRef(null)
    var PasswordRef = useRef(null)


    return (

        <View style={styles.parent}>

            {props.isLoading && <Loader />}

            {/* clear old value in input field when leave screen  */}
            
            <NavigationEvents
                onDidBlur={() => clearInputs({ setName ,setEmail, setPassword })}
            />

            <ImageBackground source={require('../../assets/icons/left-art.png')} imageStyle={styles.topImage} style={styles.header} >
                <Image
                    resizeMode='contain'
                    style={styles.logo}
                    source={require('../../assets/icons/logo.png')}
                />
            </ImageBackground>

            <MarginV v={15} />

            <View style={styles.body}>

                <View>

                    <KeyboardAwareScrollView>

                        <LargText title="SIGN UP" color="#fff" />

                        <MarginV v={5} />

                        <SmallText title="Please sign up to continue using the app " color="#fff" />

                        <MarginV v={10} />

                        <InputField
                            label="Name"
                            value={name}
                            returnKeyType='next'
                            keyboardType='default'
                            onChangeText={(value) => { setName(value) }}
                            blurOnSubmit={false}
                            onSubmitEditing={() => { EmailRef.focus(); }}
                        />
                        
                        <MarginV v={5} />
                        
                        <InputField
                            label="Email"
                            value={email}
                            autoCapitalize='none'
                            returnKeyType='next'
                            keyboardType='default'
                            onChangeText={(value) => { setEmail(value) }}
                            blurOnSubmit={false}
                            inputRef={(input) => { EmailRef = input }}
                            onSubmitEditing={() => { PasswordRef.focus(); }}
                        />

                        <MarginV v={5} />

                        <PasswordField
                            label="Password"
                            autoCapitalize='none'
                            onChangeText={(value) => { setPassword(value) }}
                            value={password}
                            keyboardType='default'
                            inputRef={(input) => { PasswordRef = input }}
                            onSubmitEditing={() => { props.signup(name, email, password) }}
                        />
                      
                        <MarginV v={10} />

                        <View style={styles.buttonContainer}>
                            <PrimaryButton title="Create Account" titleStyle={{ fontSize: 16 }} action={() => { props.signup(name,email,password) }} />
                        </View>

                        <MarginV v={10} />

                        <TouchableOpacity onPress={() => { props.navigation.navigate('Login'); }} style={styles.signinLink}>
                            <SmallText title="Already have an account?" color="#fff" /><SmallText title=" Sign In" color="#FF7B00" />
                        </TouchableOpacity>

                    </KeyboardAwareScrollView>

                    <MarginV v={10} />

                </View>

            </View>

        </View>
    )
}

const mapStateToProps = state => ({
    isLoading: state.auth.isLoading,
    userData: state.auth.userData,
    error: state.auth.error
})

const mapDispatchToProps = dispatch => ({
    signup: (name, email, password) => dispatch(actions.signup({ name ,email, password }))
})

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
