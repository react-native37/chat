// this screen check user logged in , if logged in then navigate to home screen else goto login screen

import React, { useEffect } from "react";
import {  View, Image, Text } from "react-native";
import { connect } from 'react-redux';

import styles from "./styles";

const AuthLoading = (props) => {

    useEffect(() => {

        setTimeout(() => { _bootstrapAsync(); }, 1500);

    }, [])

//handler check user logged in or not , if yes goto home screen

    _bootstrapAsync = async () => {

        const { navigation } = props;
        
        navigation.navigate(props.isLoggedIn ? "HomeStack" : "AuthStack");
    };


    return (

        <View style={styles.main}>

            <View style={styles.firstChield}>
                <Image
                    style={styles.topImage}
                    source={require('../../assets/icons/left-art.png')}
                />
            </View>

            <View style={styles.secondChield}>
                <Image
                    style={styles.logo}
                    source={require('../../assets/icons/logo.png')}
                />
                <Text style={styles.title}>Chat App</Text>
            </View>

        </View>
    );
}

const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn,
})

export default connect(mapStateToProps)(AuthLoading);
