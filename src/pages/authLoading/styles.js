import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   
    main: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: '#000000'
    },
    firstChield: {
        flex: 1
    },
    secondChield: {
        height: '65%',
        alignItems: 'center',

        top: 12
    },
    topImage: {
        width: 135, height: 182
    },
    logo: {
        width: 150, height: 143
    },
    title: {
        fontSize: 34,
        fontWeight: 'bold',
        color: '#ffffff',
        top: 30
    }

});

export default styles;