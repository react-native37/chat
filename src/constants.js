import {Dimensions } from 'react-native';

const { width, height } = Dimensions.get("screen");

const screenWidth=width>height?height:width;
const screenHeight = height> width ? height : width;
const isLandscap = width > height?true:false;

const userPhotoUrl = 'http://guidegoose.embedinfosoft.com/media/users_photo';
const chatApiUrl = 'https://guidegoose-chat.herokuapp.com';
//const chatApiUrl = 'http://65fc66722b45.ngrok.io';

export  { screenWidth, screenHeight, userPhotoUrl, isLandscap, chatApiUrl};