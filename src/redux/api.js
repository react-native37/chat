import { chatApiUrl } from '@app/constants'
import axios from 'axios';

const api = axios.create({
    baseURL: chatApiUrl
});


//aip.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

export default api;