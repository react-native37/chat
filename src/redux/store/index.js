import AsyncStorage from '@react-native-community/async-storage';
import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers'
import { logger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';


const middlewares = [thunk];

if (false) {
    middlewares.push(logger);
}

//whitelist is allowed to store state to local storege in persist Config

const persistConfig = {
    
    key: 'root',
    
    storage: AsyncStorage,
    
    whitelist: [
        'auth',
        'chat',
        'userList'
    ],
  
};


const persistedReducer = persistReducer(persistConfig, rootReducer);

const enhancer = compose(applyMiddleware(...middlewares));

let store = createStore(persistedReducer, enhancer)

let persistor = persistStore(store);

export {
    store,
    persistor,
};
