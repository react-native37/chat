import auth from '@app/pages/login/reducer';
import forgotPassword from '@app/pages/forgotPassword/reducer';
//import searchUser from '@app/pages/userList/searchUserReducer';
import userList from '@app/pages/userList/reducer';
import chat from '@app/pages/chatScreen/reducer';

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    auth,
    forgotPassword,
    userList,
    chat,
  //  searchUser
})

export default rootReducer;
