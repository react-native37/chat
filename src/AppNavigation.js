import { createAppContainer, createSwitchNavigator} from "react-navigation";
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// screens
import { AuthLoading, Signup, ForgotPassword, ChatScreen, UserList, Login} from './pages';


const AuthStack = createStackNavigator(
    {
        Login,
        Signup,
        ForgotPassword,
        
    },
    {
        initialRouteName:'Login',
        headerMode: 'none',
        defaultNavigationOptions: {
            ...TransitionPresets.SlideFromRightIOS,
            cardOverlayEnabled: true,
            gestureEnabled: true,
        }
    },
)

// Home screen navigation stack

const HomeStack = createStackNavigator(
    {
        UserList: UserList,
        ChatScreen:ChatScreen
    },
    {
        initialRouteName: 'UserList',
        headerMode: 'none',
        defaultNavigationOptions: {
            ...TransitionPresets.SlideFromRightIOS,
            cardOverlayEnabled: true,
            gestureEnabled: true,
        }
    },
);


export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: AuthLoading,
            AuthStack: AuthStack,
            HomeStack: HomeStack,
        
        },
        {
             initialRouteName: "AuthLoading"
        })
);
